use std::collections::HashSet;
use std::collections::LinkedList;

struct Graph {
    vertices: usize,
    adjacency_list: Vec<Vec<usize>>,
}

impl Graph {
    fn new(vertices: usize) -> Self {
        Graph {
            vertices: vertices,
            adjacency_list: vec![vec![0; vertices];vertices],
        }
    }

    fn add_edge(&mut self, origin: usize, destination: usize, val: usize) {
        self.adjacency_list[origin][destination] = val;
    }

    fn has_cycle_of_length_3(&self) -> bool {
        for u in 0..self.vertices {
            let mut visited = HashSet::new();
            let mut stack = LinkedList::new();
            stack.push_front(u);
            while let Some(current) = stack.pop_front() {
                if visited.contains(&current) {
                    return true;
                }
                visited.insert(current);
                for &neighbor in &self.adjacency_list[current] {
                    stack.push_front(neighbor);
                }
            }
        }
        false
    }

    fn is_planar(self: &Self) -> bool {
        let mut result: bool = false;
        let v = self.adjacency_list.len() as isize;
        let e = self.get_edge_count();
        if e <= 3 * v - 6 {
            result = true;
            if self.has_cycle_of_length_3() {
               if !(e <= 2 * v - 4) {
                    result = false;     
               }
            }
        }
        return result;
    }

    fn get_edge_count(self: &Self) -> isize {
        return self.adjacency_list.iter()
            .map(|row| row.iter().filter(|val| **val != 0).count() as isize
        ).sum()
    }
    
    fn get_region_count(self: &Self) -> isize {
        // V - E + R = 2
        return 2 - self.adjacency_list.len() as isize + self.get_edge_count();
    }
    
    fn display(&self) {
        println!();
        for row in &self.adjacency_list {
            for &val in row {
                if val != 0 {
                    print!("{} ", val);
                } else {
                    print!(". ");
                }
            }
            println!();
        }
        println!();
    }
}

fn main() {

    let mut graph = Graph::new(11);
    graph.add_edge(0, 0, 1);
    graph.add_edge(0, 1, 1);
    graph.add_edge(0, 2, 1);
    graph.add_edge(0, 3, 1);
    graph.add_edge(0, 4, 1);
    graph.add_edge(0, 5, 1);
    graph.add_edge(0, 6, 1);
    graph.add_edge(0, 7, 1);
    graph.add_edge(0, 8, 1);
    graph.add_edge(0, 9, 1);
    graph.add_edge(0, 10, 1);
    graph.add_edge(10, 0, 1);
    graph.add_edge(10, 1, 1);

    assert_eq!(graph.get_edge_count(), 13);
    assert_eq!(graph.get_region_count(), 4);
    graph.display();
    println!("Has cycle of lenght 3? {}", graph.has_cycle_of_length_3());
    println!("Is planar? {}", graph.is_planar());

    let mut graph = Graph::new(9);
    graph.add_edge(0, 0, 1);
    graph.add_edge(0, 1, 1);
    graph.add_edge(0, 2, 1);
    graph.add_edge(0, 3, 1);
    graph.add_edge(0, 4, 1);
    graph.add_edge(0, 5, 1);
    graph.add_edge(0, 6, 1);
    graph.add_edge(0, 7, 1);
    graph.add_edge(0, 8, 1);
    graph.add_edge(8, 0, 1);
    graph.add_edge(8, 1, 1);
    graph.add_edge(8, 2, 1);
    graph.add_edge(8, 3, 1);
    graph.add_edge(8, 4, 1);
    graph.add_edge(8, 5, 1);

    assert_eq!(graph.get_edge_count(), 15);
    assert_eq!(graph.get_region_count(), 8);
    graph.display();
    println!("Has cycle of lenght 3? {}", graph.has_cycle_of_length_3());
    println!("Is planar? {}", graph.is_planar());
}
